## 1.1.6

- Add more fonts

## 1.1.5

- Add scss, css and html `formatOnSave` config

## 1.1.4

- Add typescript `formatOnSave` config

## 1.1.3

- Remove `locales.json`
- Add `files.watcherExclude`

## 1.1.2

Format on save in Vue files

## 1.1.1

Change `color theme` and `icon theme`

## 1.1.0

Add VS Code files

## 1.0.0

Add vim files for windows
